//webpack is needed in order to make import statement functional
//execCode() definition location seems to be right
//Login-Logout needs some attention

import data from "@solid/query-ldflex";

async function getName(webId) {
  const person = data[webId];
  const name = await person['http://xmlns.com/foaf/0.1/name'];
  return (name && name.termType === 'Literal' && name.datatype.value === 'http://www.w3.org/2001/XMLSchema#string')
    ? name.value
    : null;
}

async function execCode() {

var query="https://testpro.solidweb.org/profile/card#me";
var result=getName(query);
console.log(result);
}

